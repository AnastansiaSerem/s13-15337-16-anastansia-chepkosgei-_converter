package com.nav.converter;

import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;

public class MainActivity extends AppCompatActivity {
    EditText mlEditText, inchesEditText;
    TextView mlTextView, inchesTextView;
    Button convertBtn, exitBtn;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        mlEditText = findViewById(R.id.millimeteredt);
        inchesEditText = findViewById(R.id.inchesedt);

        mlTextView = findViewById(R.id.Millimetertxt);
        inchesTextView = findViewById(R.id.inchestxt);
        convertBtn = findViewById(R.id.convertbtn);
        exitBtn = findViewById(R.id.exitbtn);


        convertBtn.setOnClickListener(new View.OnClickListener() {

            @Override
            public void onClick(View v) {
                Double millimeter;
                Double inches;
                millimeter = Double.valueOf(mlEditText.getText().toString());
                inches = millimeter / 25.4;

                inchesEditText.setText(inches.toString());

            }
        });
        exitBtn.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                finish();
                System.exit(0);


            }
        });


    }


}
